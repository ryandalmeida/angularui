import { Component } from '@angular/core';
import {HttpClient} from '@angular/common/http';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private http: HttpClient) { }

  title = 'app';
  msg;

  getInfo() { 
 
    // let options = { 
    //   headers: new HttpHeaders().set('Authorization', 'Bearer ' + `${this.appService.allTokens.access_token}`) 
    // }; 
 console.log('in method');
    this.http.get('http://elb2-462503433.us-east-2.elb.amazonaws.com/',{responseType: 'text' as 'json'}).subscribe(response => { 
      console.log(response); 
      this.msg = response;
    }) 
  }
}
